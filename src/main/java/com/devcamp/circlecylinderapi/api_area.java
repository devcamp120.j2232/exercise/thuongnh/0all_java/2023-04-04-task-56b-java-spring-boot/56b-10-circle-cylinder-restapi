package com.devcamp.circlecylinderapi;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class api_area {
    // http://localhost:8080/circle-area?inputRadius=4  

    @CrossOrigin
    @GetMapping("/circle-area")
    public double area(@RequestParam(value="inputRadius", defaultValue = "1") double input) {
        //thực hiện khởi tạo đối tượng hình tròn 
        Circle hinh_tron = new Circle(input);
        return hinh_tron.getArea();
    }


   // http://localhost:8080/cylinder-volume?inputRadius=2&inputHeight=2
    @GetMapping("/cylinder-volume")
    public double cylinder(@RequestParam(value="inputRadius", defaultValue = "1") double input,
    @RequestParam(value="inputHeight", defaultValue = "1") double height
    ) {
        //thực hiện khởi tạo đối tượng hình trụ
        Cylinder hinh_tru = new Cylinder(input, height);
        return hinh_tru.getVolume();
    }
}
